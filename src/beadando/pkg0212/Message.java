/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beadando.pkg0212;

/**
 *
 * @author mahli
 */
public class Message {
    private String body;
    private User sender;
    private User recipient;
    
    Message(User pSender,User pRecipient, String pBody) {
        this.sender = pSender;
        this.recipient = pRecipient;
        this.body = pBody;
    }
    
    @Override
    public String toString(){
        return this.sender.getDisplayName() + " küldi következőnek: " + this.recipient.getDisplayName() + "\n --- \n" + this.body + "\n --- \n";
    }
}
