package beadando.pkg0212;

import java.util.HashSet;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author mahli
 */
public class User {
    private String emailAddress;
    private String displayName;
    private Set<Message> incoming;    
    private Set<Message> outgoing;

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Set<Message> getIncoming() {
        return incoming;
    }

    public Set<Message> getOutgoing() {
        return outgoing;
    }
    
    
    User(String pEmailAddress, String pDisplayName) {
        this.emailAddress = pEmailAddress;
        this.displayName = pDisplayName;
        this.incoming = new HashSet<>();
        this.outgoing = new HashSet<>();
    }
    
    public void ujFogadott(Message m) {
        incoming.add(m);
    }
    
    public void ujKuldott(Message m) {
        outgoing.add(m);
    }
    
    
    @Override
    public String toString(){
        return "(Név: " + this.displayName + ", E-Mail: " + this.emailAddress + ")";
    }
}
