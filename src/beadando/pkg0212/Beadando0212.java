/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package beadando.pkg0212;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

public class Beadando0212 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        
        User currentUser = null;
        Map<String, User> users = new TreeMap<>();
        users.put("kecske@sajt.com",new User("kecske@sajt.com", "Jóschef"));
        users.put("vereb@ember.com",new User("vereb@ember.com", "VereBRobI"));
        users.put("gsfgsdf@gfsdgsf.asdf",new User("gsfgsdf@gfsdgsf.asdf", "asfgfsdgsdfaghfas"));
        
        users.get("kecske@sajt.com").ujFogadott(new Message(users.get("vereb@ember.com"),users.get("kecske@sajt.com"),"sdfasdjilgasdflghsdfkl"));
        users.get("kecske@sajt.com").ujFogadott(new Message(users.get("gsfgsdf@gfsdgsf.asdf"),users.get("kecske@sajt.com"),"sdfasdjilgasdflghsdfkl"));
        users.get("kecske@sajt.com").ujKuldott(new Message(users.get("kecske@sajt.com"),users.get("vereb@ember.com"),"sdfasdjilgasdflghsdfkl"));
        System.out.println(users);
        
        String option = null;
        option = "2";
        boolean login = false;
        String inputEmail = "asd";
        do {
            Scanner sc = new Scanner(System.in);

            while(!users.containsKey(inputEmail) && inputEmail.length() != 0){
                
                System.out.println("Kérek egy e-mail címet: ");
                inputEmail = sc.nextLine();
                if (users.containsKey(inputEmail)){
                    login = true;
                    currentUser = users.get(inputEmail);
                }
            }
            
            if (login) {
                System.out.println("Bejövők: ");
                System.out.println(currentUser.getIncoming());
                System.out.println("Kimenők: ");
                System.out.println(currentUser.getOutgoing());

                System.out.println("\n"
                        + "Válassz:\n"
                        + "1. Új üzenet írása\n"
                        + "2. Kilépés\n");
                option = sc.nextLine();
                if ("1".equals(option)) {
                    String recipient = "";
                    while (!users.containsKey(recipient)){
                        System.out.println("Kinek megy az üzenet?");
                        recipient = sc.nextLine();
                    }
                    
                    System.out.println("Mi az üzenet?");
                    String body = sc.nextLine();
                    
                    users.get(recipient).ujFogadott(new Message(users.get(currentUser.getEmailAddress()),users.get(recipient),body));
                    users.get(currentUser.getEmailAddress()).ujKuldott(new Message(users.get(currentUser.getEmailAddress()),users.get(recipient),body));
                    
                }else{
                    inputEmail = "asd";
                }
            }
        } while (login);
    }
    
    
}
